mkdir -p build
nasm bootloader.asm -f bin -o build/bootloader.bin
nasm brick-racing.asm -f bin -o build/brick-racing.bin

dd if=build/bootloader.bin of=build/image.img bs=512 count=1
dd if=build/brick-racing.bin of=build/image.img bs=512 count=4 seek=1
