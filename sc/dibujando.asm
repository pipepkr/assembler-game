;dibuja en pantalla
%macro Dibuja 4
	push dx
	push cx
	push bx
	push ax

	;moviendo el cursor
	mov dh, %2	;fila

	;puesto que son 2 caracteres en horizontal multiplicamos
	mov al, %1
	mov dl, 2
	mul dl
	mov dl, al	;columna

	mov bh, 0		;pagina 0
	mov ah, 02h		;02h situa el cursor en la celda
	int 10h

	mov bh, 0    ;página 0
	mov bl, %4  ;atributo color

	mov cx, 2	 ;repeticiones

	mov al, %3  ;caracter
	mov ah, 09h	 ;09h escribe el caracter
	int 10h

	pop ax
	pop bx
	pop cx
	pop dx
%endmacro

;dibuja auto
%macro Auto 2	;x e y
	mov ah, %1
	mov al, %2
	add ah, 2
	add al, 1
	Dibuja ah,al,219,0b0100
	mov ah, %1
	mov al, %2
	add ah, 1
	add al, 2
	Dibuja ah,al,186,0b1000
	mov ah, %1
	mov al, %2
	add ah, 2
	add al, 2
	Dibuja ah,al,219,0b0100
	mov ah, %1
	mov al, %2
	add ah, 3
	add al, 2
	Dibuja ah,al,186,0b1000
	mov ah, %1
	mov al, %2
	add ah, 2
	add al, 3
	Dibuja ah,al,219,0b0100
	mov ah, %1
	mov al, %2
	add ah, 1
	add al, 4
	Dibuja ah,al,186,0b1000
	mov ah, %1
	mov al, %2
	add ah, 3
	add al, 4
	Dibuja ah,al,186,0b1000
%endmacro

;dibuja auto
%macro Enemy 2	;x e y
	mov ah, %1
	mov al, %2
	add ah, 2
	add al, 4
	Dibuja ah,al,219,0b0001
	mov ah, %1
	mov al, %2
	add ah, 1
	add al, 3
	Dibuja ah,al,186,0b1000
	mov ah, %1
	mov al, %2
	add ah, 2
	add al, 3
	Dibuja ah,al,219,0b0001
	mov ah, %1
	mov al, %2
	add ah, 3
	add al, 3
	Dibuja ah,al,186,0b1000
	mov ah, %1
	mov al, %2
	add ah, 2
	add al, 2
	Dibuja ah,al,219,0b0001
	mov ah, %1
	mov al, %2
	add ah, 1
	add al, 1
	Dibuja ah,al,186,0b1000
	mov ah, %1
	mov al, %2
	add ah, 3
	add al, 1
	Dibuja ah,al,186,0b1000
%endmacro

;dibuja muro
%macro Muro 1	;estado (1,2,3)
	mov bx, %1	;estado
	mov cx, 7	;repeticiones del loop

	Muros:

		push cx ;guardamos cantidad de repeticiones del loop anterior

		mov cx, 3
		ladrillo:
			mov al, bl
			add al, cl
			Dibuja 15,al,176,0b0010
			Dibuja 25,al,176,0b0010
		loop ladrillo
		add bx, 4

		pop cx ; recuperamos posicion del loop anterior

	loop Muros
%endmacro
