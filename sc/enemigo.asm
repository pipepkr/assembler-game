colision_enemy:
	;si x = xplayer
	mov al, [si]
	mov ah, [player]
	cmp al, ah
	jne fin_colision
	;y > yplayer
	mov al, [di] 	;posicion enemigo
	mov ah, 16		;posicion player en y -4
	cmp al, ah
	jl fin_colision ;al < ah fin_colision

	call game_over

	fin_colision:
ret

avanza_enemy:
	;aumentando la posision en Y
    mov al, [di]
    inc al
    mov [di], al ;guardando en la posision del arreglo
ret

salio_enemy:
	;if que permite saber cuando un Enemy llego al fondo
	mov al, 26
    cmp al, [di]
		je respawn_enemy
ret

respawn_enemy:
  inc	word [puntaje1]
	mov al, -9

	push dx
	push cx
	push bx
	push ax

	mov bx,50

  ;genera un valor entre 1 y 50
	call rand_x
	mov al, dl

	; si el valor es mayor a 25 , dl sera 16 , sino 20
	; al >= 25
	cmp al, 25
	jge cosa

	;else
	mov dl, 16
	jmp fincosa
	cosa:
	 mov dl, 20

	fincosa:
  ;guarda el valor de x en el arreglo
	mov [si], dl

	pop ax
	pop bx
	pop cx
	pop dx

  mov [di], al
  jmp sig_enemigo
ret
