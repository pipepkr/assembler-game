
;_____________delay ________________
delay:
	push dx
	push cx
	push ax

	mov cx, 2      ;HIGH WORD.
	mov dx, 0A120h ;LOW WORD.
	mov ah, 86h    ;WAIT.
	int 15h

	pop ax
	pop cx
	pop dx
ret