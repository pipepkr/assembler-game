rand_x:				; genera un numero random entre 1 y bx , guarda el resultado en dx
	mov	ah, 0x00
	int	0x1A		; toma el tiempo del sistema
	mov	ax, dx		; mueve la parte baja de dx en ax para la division
	xor	dx, dx		; limpia dx
	div	bx		;  ax en bx y guarda el resto en dx
	inc	dx


	ret
