game_over:
	mov al, 10h  ;modo de video texto 80x25
	mov ah, 00h	 ;establece el modo de video y borra la pantalla
	int 10h

	push dx
	push bx
	push ax
  	push si

  	mov dl, 35
	mov dh, 12
	mov si, fin
	call print_string

	tecla_over:
	mov	ah, 0x00
	int	0x16

	cmp al,' '
	jnz tecla_over


	;reset enemy
	mov cx, 3
	mov di, enex
	mov byte [di], 16
	inc di
	mov byte [di], 20
	inc di
	mov byte [di], 16

	mov si, eney
	mov byte [si], 0
	inc si
	mov byte [si], -12
	inc si
	mov byte [si], -26

	jmp main
  	

  	pop si
	pop ax
	pop bx
	pop dx
ret