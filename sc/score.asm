;-----------Escribir puntaje----------
score:
	push dx
	push bx
	push ax
  push si

  mov dl, 1
	mov dh, 1
	mov si, puntaje
  
	call print_string
	mov	ax, [puntaje1]	; move the score into ax
	call	print_int


  pop si
	pop ax
	pop bx
	pop dx
ret

; DX cursor position
move_cursor:
  mov ah, 0x02
  xor bh, bh
  int 0x10
ret

; AL character
print_char:
  mov ah, 0x0E	;t ell BIOS that we need to print one charater on screen
  mov bh, 0x00	; page number
  mov bl, 0x07	; text attribute 0x07 is lightgrey font on black background
  int 0x10
 ret

print_string:
  push ax
  push si
  call move_cursor
.loop:
  mov al, [si]
  cmp al, 0x00
  je .done
  call print_char
  inc si
  jmp .loop
.done:
  pop si
  pop ax
 ret

 print_int:			; print the int in ax
 	push	bp		; save bp on the stack
 	mov	bp, sp		; set bp = stack pointer

 push_digits:
 	xor	dx, dx		; clear dx for division
 	mov	bx, 10		; set bx to 10
 	div	bx		; divide by 10
 	push	dx		; store remainder on stack
 	test	ax, ax		; check if quotient is 0
 	jnz 	push_digits	; if not, loop

 pop_and_print_digits:
 	pop	ax		; get first digit from stack
 	add	al, '0'		; turn it into ascii digits
 	call	print_char	; print it
 	cmp	sp, bp		; is the stack pointer is at where we began?
 	jne	pop_and_print_digits ; if not, loop
 	pop	bp		; if yes, restore bp
 	ret
