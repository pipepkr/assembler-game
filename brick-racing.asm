jmp main
%include "./sc/dibujando.asm"
%include "./sc/delay.asm"
%include "./sc/score.asm"
%include "./sc/teclado.asm"
%include "./sc/player.asm"
%include "./sc/enemigo.asm"
%include "./sc/random.asm"
%include "./sc/gameover.asm"

section .data
puntaje db "Score: ",0
fin db "GAME OVER",0
enex db 16,20,16
eney db 0,-12,-25
res db 1


section .bss
; keyboard
 key_pressed resb 1
 player resb 1
 puntaje1 resb 1


section .text
main:
	mov ax, 16
	mov [player], ax ;primera posision del player
	
	cero:
	mov dx, -4

	stan:
		mov si, enex ;primera posision del arreglo
		mov di, eney ;primera posision del arreglo

		mov al, 10h  ;modo de video texto 80x25
		mov ah, 00h	 ;establece el modo de video y borra la pantalla
		int 10h



    mov cx, 2
    loopstart:

    call avanza_enemy

    call salio_enemy

    call colision_enemy

    ;dibujando enemigo
    Enemy [si], [di]	;posision en x e y para el enemigo (de acuerdo a los arreglos)

    ;pasamos al siguiente enemigo
 	sig_enemigo:
    dec cx
    add si, 1
    add di, 1
    cmp cx, -1
    jnz loopstart



		;dibujando los muros
		Muro dx
		add dx, 1

		;dibujando al personaje
		call player_pos
		;dibujando el puntaje
		call score
		;agregando retraso
		call delay

		cmp dx, 0
		je cero

	jmp stan
